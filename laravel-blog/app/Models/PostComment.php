<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    public function comment(){
    return $this->belongsto('App\Models\Post');
   }

   public function user(){
    return $this->belongsTo('App\Models\User');
   }
}
