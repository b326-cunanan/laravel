@extends('layouts.app')


@section('content')
    <div class="card col-6 mx-auto">
        <div class="card-body">
        <h2 class="card-title">{{$post->title}}</h2>
        <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
        <p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
        <p class="card-subtitle text-muted mb-3">Updated at: {{$post->updated_at}}</p>
        <h4>Content:</h4>
        <p class="card-text">{{$post->body}}</p>
        
        <p class="mt-4">Likes: {{$post->likes->count()}} | Comments: {{$post->comments->count()}}</p>
        
        @if(Auth::user())
        @if(Auth::id() != $post->user_id)
            <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                @method('PUT')
                @csrf
                @if($post->likes->contains('user_id', Auth::id()))
                <button class="btn btn-danger">Unlike</button>
                @else
                    <button class="btn btn-success">Like</button>

                @endif
            </form>
        @endif
        @endif
