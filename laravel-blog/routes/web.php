<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Models\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $posts = Post::all();
    return view('welcome')->with('posts', $posts);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// This route is for logging out
Route::get('/logout', [PostController::class, 'logout']);



// This route is for creation of new post:
Route::get('/posts/create', [PostController::class, 'createPost']);

// This route is for saving the post on our database:
Route::post('/posts', [PostController::class, 'savePost']);


// This route is for the list of post on our database:
Route::get('/posts', [PostController::class, 'showPosts']);


// Define a route that will return a view containing only the authenticated user's post
Route::get('/myPosts', [PostController::class, 'myPosts']);


// define a route wherein a view showing a specific post with a matching URL parameter ID will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show'])->name('post.show');

// Edit post and Update post
Route::get('/posts/{id}/edit', [PostController::class, 'edit'])->name('post.edit');

Route::put('/posts/{id}', [PostController::class, 'update'])->name('post.update');

// Route to archive a post
Route::delete('/posts/{id}', [PostController::class, 'archive'])->name('post.archive');

// Define a web route that will call the function for liking and unliking a specific post:
Route::put('/posts/{id}/like', [PostController::class, 'like']);

// Comment on a post
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);

